#!/usr/bin/python3

# Builds the rfc-queue.txt file based on available files and queue2.xml.

import xml.etree.ElementTree as ET
import os

ns = {'queue': 'http://www.rfc-editor.org/rfc-editor-queue'}

queue = ET.parse('../extra/queue2.xml')

with open('rfc-queue.txt', 'w') as out:
  for draft in queue.findall('./queue:section/queue:entry/queue:draft', ns):
    have = "" if os.path.exists('../extra/%s' % draft.text) else "[missing] ";
    print("%s%s" % (have, draft.text), file=out)

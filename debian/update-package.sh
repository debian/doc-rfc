#! /bin/bash

set -e

me=${0%.sh}
me=${me##*/}

(

mkdir -p tmp
cd tmp

rsync --progress -vzrltD --partial --stats \
    ftp.rfc-editor.org::rfc-ed-all/tar/RFC-all.tar.gz .

rsync --progress -vzrltD --partial --stats \
    ftp.rfc-editor.org::rfcs/rfc-index.txt ::rfc-index.xml .

date="$( env - ls -l --full RFC-all.tar.gz | \
         awk '{ print $6, " ", $7, " ", $8; }' | sed 's/\.000*//' )"
date2=$( date -u -d "$date" "+%Y%m%d" )

touch -d "$date" $date2

ln -sf RFC-all.tar.gz doc-rfc_${date2}.orig.tar.gz

URLS="
http://www.rfc-editor.org/queue2.xml
http://www.rfc-editor.org/queue.html
http://www.rfc-editor.org/
"
wget -N $URLS

# Extract the links to the actual drafts and save their filenames
../debian/parsequeue.py queue2.xml > $me.include

mkdir -p extra/

# and now sync the respective files
rsync --progress -vzrltD --partial --stats \
    --include-from $me.include --exclude '*' \
    rsync.ietf.org::internet-drafts/ extra/

mv index.html queue.html queue2.xml \
   rfc-index.txt rfc-index.xml $me.include \
   extra/

tar cvzf doc-rfc_${date2}.orig-extra.tar.gz -C extra .

) 2>&1 | tee $me.log

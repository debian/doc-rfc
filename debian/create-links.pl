#! /usr/bin/perl -w

# Create symlinks from the actual directories into the
# usr/share/doc/RFC/links common directory, so that all RFC files can
# be seen in the same place.

use strict;

my $dirs = `ls -d */usr/share/doc/RFC`;

for my $dir (split /\s+/, $dirs) {
    # Don't create symlinks in tmp, only in the per-package
    # directories.
    next if ($dir =~ m@^tmp/@ );
    print "$dir ...\n";
    mkdir "$dir/links";
    my $files = `ls $dir/*/rfc[0-9]* $dir/*/*/rfc[0-9]* 2>/dev/null`;
    for my $file (split /\s+/, $files) {
        $file =~ s[^.*/RFC/][../];
        my $file2 = $file;
        $file2 =~ s[^.*/][];
        symlink $file, "$dir/links/$file2" or die "$file: $!";
    }
}

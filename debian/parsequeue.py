#!/usr/bin/python3

## Extracts the list of document filenames from the queue2.xml file
## (http://www.rfc-editor.org/queue2.xml, and see
## http://www.rfc-editor.org/about/queue/ for details).

import xml.etree.ElementTree as ET
import sys

ns = {'queue': 'http://www.rfc-editor.org/rfc-editor-queue'}

queue = ET.parse(sys.argv[1])

for draft in queue.findall('./queue:section/queue:entry/queue:draft', ns):
  print(draft.text)
